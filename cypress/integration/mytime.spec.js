/// <reference types="cypress-xpath" />

require('cypress-xpath')

describe('Mytime', () =>{
    
    
    
    it('Search service or business', () => {
        cy.visit('https://www.mytime.com/consumers');
        cy.get('.search-container > .business-search > #search-query').type('haircut');

    });

    it('Search location', () => {
        cy.get('.location-search > #search-location');
    });

    it('Button Search on click',() => {
        cy.get('.flat-blue-btn').click();
    });

    it('Choose a Business and click',() =>{
        cy.xpath(`(//span[@itemprop="addressLocality"])[1]`).click();
    });

    it('Select All Services filter from the left panel',() =>{
        cy.get('.service > .filter-form-selection > .selected > .option-text').click();
    });

    it('Select second staff from the staff filter in the left side panel ', () =>{
        cy.get('span:contains(Nancy Rodriguez)').click()
        ;
    });

    it('Click the Book button for the service',() =>{
        
        cy.get('.right-col > .my-button').click();

    });

    it('Press select time in the add-on modal opened',()=> {
        cy.xpath(`(//button[@data-automation="cart.selectTime"])[1]`).click();
    });

    it('Verify that user is presented with at least 2 entries',() =>{
        cy.get(':nth-child(1) > .left-col > .opentime-title').and(cy.get(':nth-child(2) > .left-col > .opentime-title'));
    });

    it('Verify service displayed right side panel is the one selected in the step before',() =>{
        cy.get('span:contains(Haircut)');   
    });

    it('Verify price displayed right side panel is the one selected in the step before',() =>{
        cy.get('span:contains($50.00)');    
    });
    
    it('Verify staff selected is staff chosen before',() =>{
        cy.get('span:contains(Nancy Rodriguez)')     
    });
    





});